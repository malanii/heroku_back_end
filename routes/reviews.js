const express = require("express");
const router = express.Router();
const passport = require("passport"); // multer for parsing multipart form data (files)

//Import controllers
const {
    addReview,
    getReviews
} = require("../controllers/reviews");

// @route   POST /colors
// @desc    Create new color
// @access  Private
router.post(
    "/",
    // passport.authenticate("jwt-admin", { session: false }),
    addReview
);



// @route   GET /colors
// @desc    GET existing colors
// @access  Public
router.get("/", getReviews);

module.exports = router;
