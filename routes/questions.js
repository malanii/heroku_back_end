const express = require("express");
const router = express.Router();
const passport = require("passport"); // multer for parsing multipart form data (files)

//Import controllers
const {
    addQuestion,
    getQuestions
} = require("../controllers/questions");

// @route   POST /sizes
// @desc    Create new size
// @access  Private
router.post(
    "/",
    // passport.authenticate("jwt-admin", { session: false }),
    addQuestion
);

// @route   PUT /sizes/:id
// @desc    Update existing size
// @access  Private


// @route   GET /sizes
// @desc    GET existing sizes
// @access  Public
router.get("/", getQuestions);

module.exports = router;
