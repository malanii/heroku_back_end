const express = require("express");
const router = express.Router();
const passport = require("passport");

//Import controllers
const {
  placeOrder,
  // updateOrder,
  // cancelOrder,
  // deleteOrder,
  // getOrders,
  getOrders
} = require("../controllers/orders");

router.post("/", placeOrder);



// router.put(
//   "/:id",
//   passport.authenticate("jwt", { session: false }),
//   updateOrder
// );
//
//
// router.put(
//   "/cancel/:id",
//   passport.authenticate("jwt", { session: false }),
//   cancelOrder
// );
//
//
// router.delete(
//   "/:id",
//   passport.authenticate("jwt", { session: false }),
//   deleteOrder
// );
//
//
// router.get("/", passport.authenticate("jwt", { session: false }), getOrders);

router.get(
  // "/:orderNo",
  // passport.authenticate("jwt", { session: false }),
  getOrders
);

module.exports = router;
