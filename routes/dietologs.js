const express = require("express");
const router = express.Router();
const passport = require("passport");
const multer = require("multer"); // multer for parsing multipart form data (files)
const fse = require("fs-extra");

//Import controllers
const {
    addDietolog,
    getDietologs,
    getDietologById,
    getDietologsFilterParams,
    searchDietologs
} = require("../controllers/dietologs");

// Configurations for multer
const storage = multer.diskStorage({
    // Destination, where files should be stored (image url)
    destination: function(req, file, cb) {
        var newDestination = req.headers.path; // We sen image url in header ("path"), when making axios request
        fse.mkdirsSync(newDestination); // We creating folder in destination, specified in headers "path"
        cb(null, newDestination); // Saving file
    },

    filename: function(req, file, cb) {
        cb(null, file.originalname); // We accept original file-name
    }
});

const fileFilter = (req, file, cb) => {
    // Accept file (only jpeg/jpg/png)
    if (
        file.mimetype === "image/jpeg" ||
        file.mimetype === "image/png" ||
        file.mimetype === "image/jpg"
    ) {
        cb(null, true);
    } else {
        // reject file (if not jpeg/jpg/png)
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 3 // Max size 5MB
    },
    fileFilter: fileFilter
});


router.post(
    "/",
    // passport.authenticate("jwt-admin", { session: false }),
    addDietolog
);


router.get("/", getDietologs);

router.get("/filter", getDietologsFilterParams);

router.post("/search", searchDietologs);

router.get("/:itemNo", getDietologById);

module.exports = router;
