const nodemailer = require("nodemailer");
const keys = require("../config/keys");
const getConfigs = require("../config/getConfigs");

module.exports = async (email, orderNo,  res) => {
    const configs = await getConfigs();

    //authorization for sending email
    let transporter = nodemailer.createTransport({
        service:
            process.env.NODE_ENV === "production"
                ? configs.production.email.mailService
                : configs.development.email.mailService,
        auth: {
            user:
                process.env.NODE_ENV === "production"
                    ? configs.production.email.mailUser
                    : configs.development.email.mailUser,
            pass:
                process.env.NODE_ENV === "production"
                    ? configs.production.email.mailPassword
                    : configs.development.email.mailPassword
        }
    });

    const mailOptions = {
        from:
            process.env.NODE_ENV === "production"
                ? configs.production.email.mailUser
                : configs.development.email.mailUser,
        to: email,
        subject: 'health food',
        // subject: orderNo,
        // html: letterHtml
        // html:"<p>Спасибо за заказ!</p><p>Номер вашего заказа "+orderNo+"</p><p>Менеджер свяжется с вами в течении 1 часа</p>"
        html:"<p>Спасибо за заказ!</p><p>Номер вашего заказа: "+orderNo+"</p><p>Менеджер свяжется с вами в течении 1 часа.</p>"
    };

    const result = await transporter.sendMail(mailOptions);

    return result;
};
