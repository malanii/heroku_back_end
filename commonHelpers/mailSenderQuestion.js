const nodemailer = require("nodemailer");
const keys = require("../config/keys");
const getConfigs = require("../config/getConfigs");

module.exports = async (userEmail, userQuestion, questionerName, res) => {
    const configs = await getConfigs();

    //authorization for sending email
    let transporter = nodemailer.createTransport({
        service:
            process.env.NODE_ENV === "production"
                ? configs.production.email.mailService
                : configs.development.email.mailService,
        auth: {
            user:
                process.env.NODE_ENV === "production"
                    ? configs.production.email.mailUser
                    : configs.development.email.mailUser,
            pass:
                process.env.NODE_ENV === "production"
                    ? configs.production.email.mailPassword
                    : configs.development.email.mailPassword
        }
    });

    const mailOptions = {
        from:
            process.env.NODE_ENV === "production"
                ? configs.production.email.mailUser
                : configs.development.email.mailUser,
        to: 'miramis134@gmail.com',
        subject: 'вопрос от пользователя',
        // html: letterHtml
        html:"<p>Ваш пришел вопрос от пользователя "+questionerName+"</p><p>Email: "+userEmail+"</p><p>"+userQuestion+"</p>"
    };

    const result = await transporter.sendMail(mailOptions);

    return result;
};
