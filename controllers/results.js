const Result = require("../models/Result");

const uniqueRandom = require("unique-random");
const rand = uniqueRandom(0, 999999);

const queryCreator = require("../commonHelpers/queryCreator");
const filterParser = require("../commonHelpers/filterParser");
const _ = require("lodash");



exports.addResult = (req, res, next) => {
    const resultFields = _.cloneDeep(req.body);

    resultFields.itemNo = rand();

    try {
        resultFields.name = resultFields.name
            .toLowerCase()
            .trim()
            .replace(/\s\s+/g, " ");

    } catch (err) {
        res.status(400).json({
            message: `Error happened on server: "${err}" `
        });
    }

    const updatedResult = queryCreator(resultFields);

    const newResult = new Result( updatedResult);

    newResult
        .save()
        .then(result => res.json(result))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};


exports.getResults = (req, res, next) => {
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

    Result.find()
        .skip(startPage * perPage - perPage)
        .limit(perPage)
        .sort(sort)
        .then(results => res.send(results))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getResultById = (req, res, next) => {
    Result.findOne({
        itemNo: req.params.itemNo
    })
        .then(result => {
            if (!result ) {
                res.status(400).json({
                    message: `Result with itemNo ${req.params.itemNo} is not found`
                });
            } else {
                res.json(result);
            }
        })
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getReceiptsFilterParams = async (req, res, next) => {
    const mongooseQuery = filterParser(req.query);
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

    try {
        const results = await Result.find(mongooseQuery)
            .skip(startPage * perPage - perPage)
            .limit(perPage)
            .sort(sort);

        const  resultsQuantity = await Result.find(mongooseQuery);

        res.json({  results,  resultsQuantity:  resultsQuantity.length });
    } catch (err) {
        res.status(400).json({
            message: `Error happened on server: "${err}" `
        });
    }
};

exports.searchResults = async (req, res, next) => {
    if (!req.body.query) {
        res.status(400).json({ message: "Query string is empty" });
    }

    //Taking the entered value from client in lower-case and trimed
    let query = req.body.query
        .toLowerCase()
        .trim()
        .replace(/\s\s+/g, " ");


    let queryArr = query.split(" ");


    let matchedResults = await Result.find({
        $text: { $search: query }
    });

    res.send(matchedResults);
};
