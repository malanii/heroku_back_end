const Question = require("../models/Question");
//
// const uniqueRandom = require("unique-random");
// const rand = uniqueRandom(0, 999999);
const sendMail = require("../commonHelpers/mailSenderQuestion");
const queryCreator = require("../commonHelpers/queryCreator");
// const filterParser = require("../commonHelpers/filterParser");
const _ = require("lodash");

exports.addQuestion = (req, res, next) => {
    if (!req.body.emailUser || !req.body.question) {
        res.status(400).json({
            message: "Subject (emailUser) and content (question) is required."
        });

        return;
    }

    const userEmail = req.body.emailUser;
    const userQuestion = req.body.question;
    const questionerName = req.body.name;

    if (!userQuestion) {
        return res.status(400).json({
            message:
                "This operation involves sending a letter to the client. Please provide field 'userQuestion' for the letter."
        });
    }

    if (!userEmail) {
        return res.status(400).json({
            message:
                "This operation involves sending a letter to the client. Please provide field 'userEmail' for the letter."
        });
    }

    else {
            const initialQuery = _.cloneDeep(req.body);
            const newQuestion = new Question (queryCreator(initialQuery));

        newQuestion
                .save()
                .then(async question => {
                    const mailResult = await sendMail(
                        userEmail,
                        userQuestion,
                        questionerName,
                        res
                    );
                    res.json({
                        question,
                        mailResult
                    });
                })
                .catch(err =>
                    res.status(400).json({
                        message: `Error happened on server: "${err}" `
                    })
                );
        }

};



// exports.addQuestion = (req, res, next) => {
//     const questionFields = _.cloneDeep(req.body);
//
//     questionFields.itemNo = rand();
//
//     try {
//         questionFields.name =  questionFields.name
//             .toLowerCase()
//             .trim()
//             .replace(/\s\s+/g, " ");
//
//     } catch (err) {
//         res.status(400).json({
//             message: `Error happened on server: "${err}" `
//         });
//     }
//
//     const updatedQuestion = queryCreator( questionFields);
//
//     const newQuestion = new Question( updatedQuestion);
//
//     newQuestion
//         .save()
//         .then(question => res.json(question))
//         .catch(err =>
//             res.status(400).json({
//                 message: `Error happened on server: "${err}" `
//             })
//         );
// };


exports.getQuestions = (req, res, next) => {
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

    Question.find()
        .skip(startPage * perPage - perPage)
        .limit(perPage)
        .sort(sort)
        .then(questions => res.send(questions))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};
