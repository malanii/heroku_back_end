const Receipt = require("../models/Receipt");

const uniqueRandom = require("unique-random");
const rand = uniqueRandom(0, 999999);

const queryCreator = require("../commonHelpers/queryCreator");
const filterParser = require("../commonHelpers/filterParser");
const _ = require("lodash");



exports.addReceipt = (req, res, next) => {
    const receiptFields = _.cloneDeep(req.body);

    receiptFields.itemNo = rand();

    try {
        receiptFields.name = receiptFields.name
            .toLowerCase()
            .trim()
            .replace(/\s\s+/g, " ");

    } catch (err) {
        res.status(400).json({
            message: `Error happened on server: "${err}" `
        });
    }

    const updatedReceipt = queryCreator(receiptFields);

    const newReceipt = new Receipt( updatedReceipt);

    newReceipt
        .save()
        .then(receipt => res.json(receipt))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};


exports.getReceipts = (req, res, next) => {
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

    Receipt.find()
        .skip(startPage * perPage - perPage)
        .limit(perPage)
        .sort(sort)
        .then(receipts => res.send(receipts))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getReceiptById = (req, res, next) => {
    Receipt.findOne({
        itemNo: req.params.itemNo
    })
        .then(receipt => {
            if (!receipt ) {
                res.status(400).json({
                    message: `Receipt with itemNo ${req.params.itemNo} is not found`
                });
            } else {
                res.json(receipt);
            }
        })
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getReceiptsFilterParams = async (req, res, next) => {
    const mongooseQuery = filterParser(req.query);
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

    try {
        const recipes = await Receipt.find(mongooseQuery)
            .skip(startPage * perPage - perPage)
            .limit(perPage)
            .sort(sort);

        const  recipesQuantity = await Receipt.find(mongooseQuery);

        res.json({  recipes,  recipesQuantity:  recipesQuantity.length });
    } catch (err) {
        res.status(400).json({
            message: `Error happened on server: "${err}" `
        });
    }
};

exports.searchReceipts = async (req, res, next) => {
    if (!req.body.query) {
        res.status(400).json({ message: "Query string is empty" });
    }

    //Taking the entered value from client in lower-case and trimed
    let query = req.body.query
        .toLowerCase()
        .trim()
        .replace(/\s\s+/g, " ");


    let queryArr = query.split(" ");


    let matchedReceipts = await Receipt.find({
        $text: { $search: query }
    });

    res.send(matchedReceipts);
};
