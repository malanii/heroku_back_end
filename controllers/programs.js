const Program = require("../models/Program");

const uniqueRandom = require("unique-random");
const rand = uniqueRandom(0, 999999);

const queryCreator = require("../commonHelpers/queryCreator");
const filterParser = require("../commonHelpers/filterParser");
const _ = require("lodash");



exports.addProgram = (req, res, next) => {
    const programFields = _.cloneDeep(req.body);

    programFields.itemNo = rand();

    try {
        programFields.name =  programFields.name
            .toLowerCase()
            .trim()
            .replace(/\s\s+/g, " ");

    } catch (err) {
        res.status(400).json({
            message: `Error happened on server: "${err}" `
        });
    }

    const updatedProgram = queryCreator( programFields);

    const newProgram = new Program( updatedProgram);

    newProgram
        .save()
        .then(program => res.json(program))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};


exports.getPrograms = (req, res, next) => {
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

   Program.find()
        .skip(startPage * perPage - perPage)
        .limit(perPage)
        .sort(sort)
        .then(programs => res.send(programs))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getProgramById = (req, res, next) => {
    Program.findOne({
        itemNo: req.params.itemNo
    })
        .then(program => {
            if (!program ) {
                res.status(400).json({
                    message: `Program with itemNo ${req.params.itemNo} is not found`
                });
            } else {
                res.json(program);
            }
        })
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getProgramsFilterParams = async (req, res, next) => {
    const mongooseQuery = filterParser(req.query);
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

    try {
        const programs = await Program.find(mongooseQuery)
            .skip(startPage * perPage - perPage)
            .limit(perPage)
            .sort(sort);

        const programsQuantity = await Program.find(mongooseQuery);

        res.json({ programs, programsQuantity: programsQuantity.length });
    } catch (err) {
        res.status(400).json({
            message: `Error happened on server: "${err}" `
        });
    }
};

exports.searchPrograms = async (req, res, next) => {
    if (!req.body.query) {
        res.status(400).json({ message: "Query string is empty" });
    }

    //Taking the entered value from client in lower-case and trimed
    let query = req.body.query
        .toLowerCase()
        .trim()
        .replace(/\s\s+/g, " ");


    let queryArr = query.split(" ");


    let matchedPrograms = await Program.find({
        $text: { $search: query }
    });

    res.send(matchedPrograms);
};
