const Dietolog = require("../models/Dietolog");

const uniqueRandom = require("unique-random");
const rand = uniqueRandom(0, 999999);

const queryCreator = require("../commonHelpers/queryCreator");
const filterParser = require("../commonHelpers/filterParser");
const _ = require("lodash");



exports.addDietolog = (req, res, next) => {
    const dietologFields = _.cloneDeep(req.body);

    dietologFields.itemNo = rand();

    try {
        dietologFields.name = dietologFields.name
            .toLowerCase()
            .trim()
            .replace(/\s\s+/g, " ");

    } catch (err) {
        res.status(400).json({
            message: `Error happened on server: "${err}" `
        });
    }

    const updatedDietolog = queryCreator(dietologFields);

    const newDietolog = new Dietolog( updatedDietolog);

    newDietolog
        .save()
        .then(dietolog => res.json(dietolog))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};


exports.getDietologs = (req, res, next) => {
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

    Dietolog.find()
        .skip(startPage * perPage - perPage)
        .limit(perPage)
        .sort(sort)
        .then(dietologs => res.send(dietologs))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getDietologById = (req, res, next) => {
    Dietolog.findOne({
        itemNo: req.params.itemNo
    })
        .then(dietolog => {
            if (!dietolog ) {
                res.status(400).json({
                    message: `Dietolog with itemNo ${req.params.itemNo} is not found`
                });
            } else {
                res.json(dietolog);
            }
        })
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getDietologsFilterParams = async (req, res, next) => {
    const mongooseQuery = filterParser(req.query);
    const perPage = Number(req.query.perPage);
    const startPage = Number(req.query.startPage);
    const sort = req.query.sort;

    try {
        const dietologs = await Dietolog.find(mongooseQuery)
            .skip(startPage * perPage - perPage)
            .limit(perPage)
            .sort(sort);

        const dietologsQuantity = await Dietolog.find(mongooseQuery);

        res.json({ dietologs, dietologsQuantity: dietologsQuantity.length });
    } catch (err) {
        res.status(400).json({
            message: `Error happened on server: "${err}" `
        });
    }
};

exports.searchDietologs = async (req, res, next) => {
    if (!req.body.query) {
        res.status(400).json({ message: "Query string is empty" });
    }

    //Taking the entered value from client in lower-case and trimed
    let query = req.body.query
        .toLowerCase()
        .trim()
        .replace(/\s\s+/g, " ");


    let queryArr = query.split(" ");


    let matchedDietologs = await Dietolog.find({
        $text: { $search: query }
    });

    res.send(matchedDietologs);
};
