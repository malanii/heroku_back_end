const Review = require("../models/Review");
const queryCreator = require("../commonHelpers/queryCreator");
const _ = require("lodash");

exports.addReview = (req, res, next) => {
    Review.findOne({ name: req.body.name }).then(review => {
        if (review) {
            return res
                .status(400)
                .json({ message: `Review with name "${review.name}" already exists` });
        } else {
            const initialQuery = _.cloneDeep(req.body);
            const newReview = new Review(queryCreator(initialQuery));

            newReview
                .save()
                .then(review => res.json(review))
                .catch(err =>
                    res.status(400).json({
                        message: `Error happened on server: "${err}" `
                    })
                );
        }
    });
};


exports.getReviews = (req, res, next) => {
    Review.find()
        .then(review => res.json(review))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};
