const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProgramSchema = new Schema(
    {
        itemNo: {
            type: String,
            required: true
        },
        enabled: {
            type: Boolean,
            required: true,
            default: true
        },
        name: {
            type: String,
            required: true
        },
        target: {
            type: String,
            required: true
        },
        description: [
            {
                number:{
                    type: String,
                    required: true
                },
                text:{
                    type: String,
                    required: true
                }
            }

        ],
        conclusions: [
            {
               icon:{
                    type: String,
                    required: true
                },
                param:{
                    type: String,
                    required: true
                },
                text:{
                    type: String,
                    required: true
                }
            }

        ],

        imageUrls: {
            type: String,
            required: true
        },
        backgroundImg: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            required: true
        },
        minQuantity: {
            type: Number,
            required: true
        },
        maxQuantity: {
            type: Number,
            required: true
        },

        weeklyCount: [
            {
                imageUrls: {
                    type: String,
                    required: true
                },
                dayName: {
                    type: String,
                    required: true
                },
                schedule: [{
                    time: {
                        type: String,
                        required: true
                    },
                    food: {
                        type: String,
                        required: true
                    },
                    portion: {
                        type: String,
                        required: true
                    },
                }
                ]

            }
        ],
        // results: {
        //     type: String,
        //     required: true
        // },
        currentPrice: {
            type: Number,
            required: true
        },
        goals: {
            type: String,
        },
        date: {
            type: Date,
            default: Date.now
        }
    },
    {strict: false}
);

ProgramSchema.index({"$**": "text"});

module.exports = Program = mongoose.model("programs", ProgramSchema);
