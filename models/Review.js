const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ReviewSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        review: {
            type: String
        },
        rating: {
            type: Number
        },
        image: {
            type: String
        },
        date: {
            type: Date,
            default: Date.now
        }
    },
    { strict: false }
);

module.exports =  Review = mongoose.model("reviews",  ReviewSchema);
