const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const QueationSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        emailUser: {
            type: String,
            required: true
        },
        question: {
            type: String,
            required: true
        },
        date: {
            type: Date,
            default: Date.now
        }
    },
    { strict: false }
);

module.exports = Question = mongoose.model("question", QueationSchema);
