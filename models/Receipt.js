const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ReceiptSchema = new Schema(
    {
        itemNo: {
            type: String,
            required: true
        },
        enabled: {
            type: Boolean,
            required: true,
            default: true
        },
        name: {
            type: String,
            required: true
        },
       diet: {
            type: String,
            required: true
        },
        eating: {
            type: String,
            required: true
        },
        imgDishes: {
            type: String,
            required: true
        },
        rating: {
            type: Number,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        imgIngredient:
            {
                type: String,
                required: true
            }
        ,
        ingredients: {
            type: String,
            required: true
        },
        time: {
            type: String,
            required: true
        },
        difficulty: {
            type: Number,
            required: true
        },
        portion: {
            type: String,
            required: true
        },
        nutritionFacts: {
            type: String,
            required: true
        },
        direction: {
            type: String,
            required: true
        },
        date: {
            type: Date,
            default: Date.now
        }
    },
    { strict: false }
);

ReceiptSchema.index({ "$**": "text" });

module.exports = Receipt = mongoose.model("receipts",  ReceiptSchema);
