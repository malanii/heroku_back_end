const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DietologSchema = new Schema(
    {
        itemNo: {
            type: String,
            required: true
        },
        enabled: {
            type: Boolean,
            required: true,
            default: true
        },
        name: {
            type: String,
            required: true
        },
        imageUrls: {
            type: String,
            required: true
        },
        sex: {
            type: String,
            required: true
        },
        currentPrice: {
            type: Number,
            required: true
        },
        previousPrice: {
            type: Number
        },
        experience: {
            type: Number,
            required: true
        },
        reviews: {
            type: Number,
            required: true
        },
        rating: {
            type: Number,
            required: true
        },

        qualification: {
            type: String,

        },
        specialization: {
            type: String,
        },
        description: {
            type: String,
            required: true
        },
        study: [
            {
                type: String,
                required: true
            }
        ],
        progress: [
            {
                type: String,
                required: true
            }
        ],
        procedures: [
            {
                type: String,
                required: true
            }
        ],
        treatment: [
            {
                type: String,
                required: true
            }
        ],
        date: {
            type: Date,
            default: Date.now
        }
    },
    { strict: false }
);

DietologSchema.index({ "$**": "text" });

module.exports = Dietolog = mongoose.model("dietologs", DietologSchema);
