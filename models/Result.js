const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ResultSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        story: {
            type: String
        },
        imgBefore: {
            type: String
        },
        imgAfter: {
            type: String
        },
        results: {
            type: String
        },
        sizes: {
            type: String
        },
        date: {
            type: Date,
            default: Date.now
        }
    },
    { strict: false }
);

module.exports = Result = mongoose.model("results", ResultSchema);
